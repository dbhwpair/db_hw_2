SELECT photos.user_id, id, COALESCE(tags_counter.num_tags, 0) AS num_tags FROM photos
LEFT OUTER JOIN (
    SELECT user_id, photo_id, COUNT(info) AS num_tags FROM tags
    GROUP BY  user_id, photo_id) AS tags_counter
ON (id = tags_counter.photo_id AND photos.user_id = tags_counter.user_id)
ORDER BY num_tags DESC, user_id, photo_id;
