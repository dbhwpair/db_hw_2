SELECT * FROM tags tags1
left outer join tags tags2 on (
    tags1.photo_id != tags2.photo_id and tags1.info = tags2.info
); 

SELECT tags1.photo_id, tags2.photo_id, count(tags2.info) num_same_tag FROM tags tags1
left outer join tags tags2 on (
    tags1.photo_id != tags2.photo_id and tags1.info = tags2.info
) 
group by tags1.photo_id, tags2.photo_id having count(tags2.info) >= 2;

SELECT tags1.photo_id as photo_id1, tags2.photo_id as photo_id2 FROM tags tags1
left outer join tags tags2 on (
    tags1.photo_id != tags2.photo_id and tags1.info = tags2.info
) 
group by tags1.photo_id, tags2.photo_id having count(tags2.info) >= 2;

select photo_id1 as photo_id from (
    SELECT tags1.photo_id as photo_id1, tags2.photo_id as photo_id2 FROM tags tags1
    left outer join tags tags2 on (
        tags1.photo_id != tags2.photo_id and tags1.info = tags2.info
    ) 
    group by tags1.photo_id, tags2.photo_id having count(tags2.info) >= 2
) as foo
group by photo_id1 having count(photo_id1) >= 1;


SELECT DISTINCT user_id, name, photo_id FROM tags
LEFT OUTER JOIN users ON (user_id = id)
WHERE photo_id IN (
    select photo_id1 as photo_id from (
        SELECT tags1.photo_id as photo_id1, tags2.photo_id as photo_id2 FROM tags tags1
        left outer join tags tags2 on (
            tags1.photo_id != tags2.photo_id and tags1.info = tags2.info
        ) 
        group by tags1.photo_id, tags2.photo_id having count(tags2.info) >= 2
    ) as foo
    group by photo_id1 having count(photo_id1) >= 1
)
ORDER BY user_id, photo_id;
