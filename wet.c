#include "wet.h"
#include <stdio.h>
#include <libpq-fe.h>
#include <assert.h>
#include <stdio.h>

/*------------------------PQconnect----------------------*/

#define DATABASE_NAME "todder"
//#define HOST_NAME "localhost"
#define HOST_NAME "csl2.cs.technion.ac.il"
#define RETURN return NULL
// #define DEBUG 

#define EXEC_OR_GOTO_CLEANUP(__query, __res, __expected_status) \
    do {\
        __res = PQexec(conn, __query); \
        if (NULL == __res) {\
            printf("memory failure during quering the database\n");\
            goto cleanUp;\
        }\
        if (PQresultStatus(__res) != __expected_status) {\
            printf("failure during quering the database\n%s\n", PQresultErrorMessage(__res));\
            goto cleanUp;\
        }\
    } while (0)

#define EXEC_QUERY_OR_GOTO_CLEANUP(__query, __res)   EXEC_OR_GOTO_CLEANUP(__query, __res, PGRES_TUPLES_OK)
#define EXEC_COMMAND_OR_GOTO_CLEANUP(__query, __res) EXEC_OR_GOTO_CLEANUP(__query, __res, PGRES_COMMAND_OK)

char connectionDetailes[256] = {'\0'};
const char * const connectionDetailsFormat = "dbname=%s host=%s user=%s password=%s";
PGconn* conn = NULL;


static void setConnectonDetails(void) {
    sprintf(connectionDetailes, connectionDetailsFormat, DATABASE_NAME, HOST_NAME, USERNAME, PASSWORD);
}

const int ERROR = -1;
const int SUCCESS = 0;

/**
 * Function: addUser
 * Writer: Jonathan
 * Notes:
 */
void* addUser(const char* name) {
    PGresult* res;
#ifdef DEBUG
    printf("at addUser\n");
    printf("got %s\n", name);
#endif
    char query[1024] = {'\0'};
    char* query2 = "SELECT MAX(id) from users" ;
    const char * const format = "INSERT INTO users VALUES ((SELECT CASE WHEN EXISTS(SELECT * FROM users) THEN max(id)+1 ELSE 0 END FROM users), '%s');" ;
    sprintf(query, format, name);
    EXEC_COMMAND_OR_GOTO_CLEANUP(query, res);
    EXEC_QUERY_OR_GOTO_CLEANUP(query2, res);
    char* max_id = PQgetvalue(res, 0, 0);
    printf(ADD_USER, max_id);

cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: addUserMin
 * Writer: Taras
 * Notes:
 */
void* addUserMin(const char* name) {
    PGresult* res;
    char cmd[1024] = {'\0'};
    const char * const cmd_format = "\
        INSERT INTO users VALUES ((\
            SELECT COALESCE(MIN(users1.id)+1, 0) FROM users users1\
            LEFT OUTER JOIN users users2 ON (users1.id + 1)  = users2.id\
            WHERE users2.id IS NULL),'%s')";
    sprintf(cmd, cmd_format, name);
    EXEC_COMMAND_OR_GOTO_CLEANUP(cmd, res);
    PQclear(res);
    char query[512] = {'\0'};
    const char * const query_format =  "SELECT * FROM users WHERE name = '%s' ORDER BY id";
    sprintf(query, query_format, name);
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    int numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(USER_HEADER);
    int row; for (row = 0; row < numRows; row++) {
        printf(USER_RESULT, PQgetvalue(res, row, 0),
                            PQgetvalue(res, row, 1));
    }
cleanUp:
    PQclear(res);
    RETURN;
}


static int numRowsInQuery(const char* query) {
    int result =  ERROR;
    PGresult* res;
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    result = PQntuples(res);
cleanUp:
    PQclear(res);
    return result;
}

static int userExists(const char* user_id) {
    char query[512] = {'\0'};
    const char * const format = "SELECT * FROM users where id=%s";
    sprintf(query, format, user_id);
    return numRowsInQuery(query) > 0;
}

static int userPhotoExists(const char* user_id, const char* photo_id) {
    char query[512] = {'\0'};
    const char * const format = "SELECT * FROM photos WHERE id=%s AND user_id=%s";
    sprintf(query, format, photo_id, user_id);
    return numRowsInQuery(query) > 0;
}

    
static int tagExists(const char* user_id, const char* photo_id, const char* info) {
    char query[512] = {'\0'};
    const char * const format = "SELECT * FROM tags WHERE photo_id=%s AND user_id=%s AND info='%s'";
    sprintf(query, format, photo_id, user_id, info);
    return numRowsInQuery(query) > 0;
}
/**
 * Function: removeUser
 * Writer: Jonathan
 * Notes:
 */
void* removeUser(const char*    id) {
    PGresult* res;
    if (!userExists(id)) {
        printf(ILL_PARAMS);
        RETURN;
    }
#ifdef DEBUG
    printf("at removeUser\n");
    printf("got %s\n", id);
#endif
    char query_1[512] = {'\0'};
    const char * const format_1 = "DELETE FROM photos WHERE user_id='%s';" ;
    char query_2[512] = {'\0'};
    const char * const format_2 = "DELETE FROM tags WHERE user_id='%s';" ;
    char query_3[512] = {'\0'};
    const char * const format_3 = "DELETE FROM users WHERE id='%s';" ;

    sprintf(query_1, format_1, id);
    sprintf(query_2, format_2, id);
    sprintf(query_3, format_3, id);

    EXEC_COMMAND_OR_GOTO_CLEANUP(query_1, res);
    EXEC_COMMAND_OR_GOTO_CLEANUP(query_2, res);
    EXEC_COMMAND_OR_GOTO_CLEANUP(query_3, res);

cleanUp:
    PQclear(res);
    RETURN;

}

/**
 * Function: addPhoto
 * Writer: Taras
 * Notes: we can save one query - userPhotoExists, ask the teacher whether we realy shuold do this.
 */
void* addPhoto(const char* user_id, const char* photo_id) {
    if (!userExists(user_id)) {
        printf(ILL_PARAMS);
        RETURN;
    }
    if (userPhotoExists(user_id, photo_id)) {
        printf(EXISTING_RECORD);
        RETURN;
    }
    char cmd[512] = {'\0'};
    const char * const format = "INSERT INTO photos VALUES (%s, %s)";
    sprintf(cmd, format, photo_id, user_id);
    PGresult* res;
    EXEC_COMMAND_OR_GOTO_CLEANUP(cmd, res);
cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: tagPhoto
 * Writer: Jonathan
 * Notes:
 */
void* tagPhoto(const char*    user_id,
               const char*    photo_id,
               const char*    info) {
    PGresult* res;
    if (!userExists(user_id)) {
        printf(ILL_PARAMS);
        RETURN;
    }
    if (tagExists(user_id, photo_id, info)) {
        printf(EXISTING_RECORD);
        RETURN;
    }
    char query[512] = {'\0'};
    const char * const format = "INSERT INTO tags VALUES (%s, %s, '%s')";
    sprintf(query, format, photo_id, user_id, info);
    EXEC_COMMAND_OR_GOTO_CLEANUP(query, res);
cleanUp:
    PQclear(res);
    RETURN;

}

/**
 * Function: photosTags
 * Writer: Taras
 * Notes:
 */
void* photosTags() {
    int numRows, row;
    PGresult*  res;
    const char * const query =  "\
        SELECT photos.user_id, id, COALESCE(tags_counter.num_tags_in_counter, 0) AS num_tags FROM photos\
        LEFT OUTER JOIN (\
            SELECT user_id, photo_id, COUNT(info) AS num_tags_in_counter FROM tags\
            GROUP BY  user_id, photo_id) AS tags_counter\
        ON (id = tags_counter.photo_id AND photos.user_id = tags_counter.user_id)\
        ORDER BY num_tags DESC, photos.user_id, id";
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(PHOTOS_HEADER);
    for (row = 0; row < numRows; row++) {
        printf(PHOTOS_RESULT, PQgetvalue(res, row, 0),
                              PQgetvalue(res, row, 1),
                              PQgetvalue(res, row, 2));
    }
cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: search
 * Writer: Jonathan
 * Notes:
 */
void* search(const char* word) {
    int numRows, row;
    PGresult*  res;
    char query[512] = {'\0'};
    const char * const  format =
            "SELECT photo_id, user_id, COUNT(photo_id) \
            FROM tags T \
            WHERE info LIKE '%%%s%%' \
            GROUP BY photo_id, user_id \
            ORDER BY count DESC, user_id ASC, photo_id DESC;";
    sprintf(query, format, word);
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(PHOTOS_HEADER);
    for (row = 0; row < numRows; row++) {
        printf(PHOTOS_RESULT, PQgetvalue(res, row, 1),
                              PQgetvalue(res, row, 0),
                              PQgetvalue(res, row, 2));
    }
cleanUp:
    PQclear(res);
    RETURN;

}

/**
 * Function: commonTags
 * Writer: Taras
 * Notes:
 */
void* commonTags(const char* k) {
    int numRows, row;
    PGresult*  res;
    char query[512] = {'\0'};
    const char * const format = "SELECT info, COUNT(photo_id) AS num_photos FROM tags GROUP BY info HAVING COUNT(photo_id) >= %s ORDER BY num_photos DESC, info";
    sprintf(query, format, k);
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(COMMON_HEADER);
    for (row = 0; row < numRows; row++) {
        printf(COMMON_LINE, PQgetvalue(res, row, 0),
                            PQgetvalue(res, row, 1));
    }
cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: mostCommonTags
 * Writer: Jonathan
 * Notes:
 */
void* mostCommonTags    (const char*    k){
    int numRows, row;
    PGresult*  res;
    char query[2048] = {'\0'};
    const char * const  format =  "SELECT  tag_info , (SELECT COUNT(*) FROM tags WHERE tag_info=tags.info) FROM ( \
 SELECT foo_1.info_1 AS tag_info, COUNT(*) AS line_number \
 FROM \
    ( \
    SELECT COUNTS.S_info AS info_1, COUNT(*) AS inner_line_num_1 \
    FROM    ( \
    SELECT DISTINCT S_info, T_count \
    FROM \
    (SELECT info AS S_info , COUNT(*) AS S_count FROM tags GROUP BY info ORDER BY info ASC) as S, \
    (SELECT info AS T_info, COUNT(*) AS T_count FROM tags GROUP BY info) as T \
    WHERE S_count <= T_count \
    ) AS COUNTS \
    GROUP BY COUNTS.S_info) AS foo_1, \
    ( \
    SELECT COUNTS.S_info AS info_2, COUNT(*) AS inner_line_num_2 \
    FROM    ( \
    SELECT DISTINCT S_info, T_count \
    FROM \
    (SELECT info AS S_info , COUNT(*) AS S_count FROM tags GROUP BY info ORDER BY info ASC) as S, \
    (SELECT info AS T_info, COUNT(*) AS T_count FROM tags GROUP BY info) as T \
    WHERE S_count <= T_count \
    ) AS COUNTS \
    GROUP BY COUNTS.S_info) AS foo_2 \
    WHERE \
    foo_1.inner_line_num_1 > foo_2.inner_line_num_2 \
    OR \
    (foo_1.inner_line_num_1 = foo_2.inner_line_num_2 \
    AND foo_1.info_1 >= foo_2.info_2) \
    GROUP BY foo_1.info_1 \
    ORDER BY line_number ASC) BLA \
    WHERE line_number <= %s;";    

    sprintf(query, format, k);
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(COMMON_HEADER);
    for (row = 0; row < numRows; row++) {
        printf(COMMON_LINE, PQgetvalue(res, row, 0),
                              PQgetvalue(res, row, 1));
    }
cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: similarPhotos
 * Writer: Taras
 * Notes:
 */
void* similarPhotos(const char* k, const char* j) {
    int numRows, row;
    PGresult*  res;
    char query[1024] = {'\0'};
    const char * const format = "\
        SELECT DISTINCT user_id, name, photo_id FROM tags\
        LEFT OUTER JOIN users ON (user_id = id)\
        WHERE photo_id IN (\
            select photo_id1 as photo_id from (\
                SELECT tags1.photo_id as photo_id1, tags2.photo_id as photo_id2 FROM tags tags1\
                left outer join tags tags2 on (\
                    tags1.photo_id != tags2.photo_id and tags1.info = tags2.info\
                )\
                group by tags1.photo_id, tags2.photo_id having count(tags2.info) >= %s\
            ) as foo\
            group by photo_id1 having count(photo_id1) >= %s\
        )\
        ORDER BY user_id, photo_id";
    sprintf(query, format, j, k);
    EXEC_QUERY_OR_GOTO_CLEANUP(query, res);
    numRows = PQntuples(res);
    if (0 == numRows) {
        printf(EMPTY);
        goto cleanUp;
    }
    printf(SIMILAR_HEADER);
    for (row = 0; row < numRows; row++) {
        printf(SIMILAR_RESULT, PQgetvalue(res, row, 0),
                               PQgetvalue(res, row, 1),
                               PQgetvalue(res, row, 2));
    }
cleanUp:
    PQclear(res);
    RETURN;
}

/**
 * Function: autoPhotoOnTagOn
 * Writer: Jonathan
 * Notes:
 */
void* autoPhotoOnTagOn  (){
    PGresult* res; 
    const char * const query = "CREATE OR REPLACE FUNCTION  add_photo_to_user() \
        RETURNS TRIGGER AS $$ \
            BEGIN \
                IF ((NEW.photo_id, NEW.user_id) NOT IN (SELECT id, user_id FROM photos)) THEN \
                    INSERT INTO photos VALUES(NEW.photo_id, NEW.user_id); \
                END IF; \
            RETURN NEW; \
            END; \
        $$ LANGUAGE plpgsql; \
        CREATE TRIGGER tag_trigger \
        BEFORE INSERT ON tags \
        FOR EACH ROW EXECUTE PROCEDURE add_photo_to_user();";
    EXEC_COMMAND_OR_GOTO_CLEANUP(query, res);
cleanUp:
    PQclear(res);
    RETURN;
}


/**
 * Function: autoPhotoOnTagOFF
 * Writer: Taras
 * Notes:
 */
void* autoPhotoOnTagOFF() {
    PGresult*  res;
    const char * const cmd = "DROP TRIGGER IF EXISTS tag_trigger ON tags";
    EXEC_COMMAND_OR_GOTO_CLEANUP(cmd, res);
cleanUp:
    PQclear(res);
    RETURN;
}

static int init(void) {
    setConnectonDetails(); 
    conn = PQconnectdb(connectionDetailes);
    if (conn == NULL || PQstatus(conn) != CONNECTION_OK) {
        printf("failure during the connection to database\n");
        conn = NULL;
        return ERROR;
    }
#ifdef DEBUG
    printf("connection successfuly established\n");
#endif
    return SUCCESS;
}

static void tearDown(void) {
    PQfinish(conn);
}

int main() {
     if (init() == SUCCESS) { 
         parseInput();
     }
     tearDown();
     return 0;
}

