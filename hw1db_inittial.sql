--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: photos; Type: TABLE; Schema: public; Owner: todder; Tablespace: 
--

CREATE TABLE photos (
    id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.photos OWNER TO todder;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: todder; Tablespace: 
--

CREATE TABLE tags (
    photo_id integer NOT NULL,
    user_id integer NOT NULL,
    info text NOT NULL
);


ALTER TABLE public.tags OWNER TO todder;

--
-- Name: users; Type: TABLE; Schema: public; Owner: todder; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.users OWNER TO todder;

--
-- Data for Name: photos; Type: TABLE DATA; Schema: public; Owner: todder
--

COPY photos (id, user_id) FROM stdin;
0	0
1	0
2	0
20	1
23	1
10	3
\.


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: todder
--

COPY tags (photo_id, user_id, info) FROM stdin;
0	0	Car
0	0	Bridge
0	0	Sky
20	1	Car
20	1	Bridge
10	3	Sky
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: todder
--

COPY users (id, name) FROM stdin;
0	Bob
1	Ben
2	Roy
3	Lev
\.


--
-- Name: photos_pkey; Type: CONSTRAINT; Schema: public; Owner: todder; Tablespace: 
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id, user_id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: todder; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (user_id, photo_id, info);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: todder; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

